from structures.queue import Queue


def bfs(nodes, values=[]):

    childs = []
    for node in nodes:
        values.append(node.value)
        childs = childs + node.childs

    if len(childs) > 0:
        return bfs(childs, values)
    return values


def bfs_q(node):
    que = Queue()

    que.put(node)
    ret = []

    while not que.isEmpty():
        item = que.get()
        for child in item.childs:
            que.put(child)
        ret.append(item.value)

    return ret
