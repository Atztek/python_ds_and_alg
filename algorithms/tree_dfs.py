from structures.stack import Stack


def dfs(node):
    ret = []
    ret.append(node.value)

    for child in node.childs:
        ret = ret + dfs(child)

    return ret


def dfs_q(node):
    que = Stack()

    que.put(node)
    ret = []

    while not que.isEmpty():
        item = que.get()
        for child in item.childs[::-1]:
            que.put(child)
        ret.append(item.value)

    return ret
