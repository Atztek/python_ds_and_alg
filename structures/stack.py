

class Stack():

    def __init__(self):
        self._list = []

    def put(self, value):
        self._list.append(value)

    def get(self):
        if self.isEmpty():
            return None
        return self._list.pop(-1)

    def isEmpty(self):
        return len(self._list) == 0
