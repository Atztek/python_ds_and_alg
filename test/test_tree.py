import unittest
from structures.tree import Node


class TestTree(unittest.TestCase):

    def test_make(self):
        root = Node(0, [
            Node(1, [
                Node(3)
            ]),
            Node(2)
        ])

        self.assertEqual(len(root.childs), 2, "Дерево создано неправильно")
