import unittest
from structures.queue import Queue


class TestQueue(unittest.TestCase):
    """Тестирование очереди"""

    def setUp(self):
        self.q = Queue()
        return super().setUp()

    def test_put(self):
        """Добавление в список"""
        self.q.put("val")
        self.assertEqual(len(self.q._list), 1,
                         "В очереди должени быть 1 элемент")

    def test_get(self):
        val = self.q.get()
        self.assertEqual(val, None, "Возращенное значение не верно")

        self.q.put(1)
        self.q.put(2)
        val = self.q.get()
        self.assertEqual(val, 1, "Возращенное значение не верно")
