import unittest
from algorithms.tree_bfs import bfs, bfs_q
from structures.tree import Node


class TestTreeBfs(unittest.TestCase):
    """Тест поиска в ширину"""

    def test_bfs(self):
        root = Node(0, [
            Node(1, [
                Node(3, [
                    Node(5)
                ]),
                Node(4)
            ]),
            Node(2)
        ])

        items_list = bfs([root])
        items_list_q = bfs_q(root)

        print(items_list)

        self.assertListEqual(
            items_list, [0, 1, 2, 3, 4, 5], "Выходной массив не соответствует")

        self.assertListEqual(
            items_list_q, [0, 1, 2, 3, 4, 5], "Выходной массив не соответствует")
