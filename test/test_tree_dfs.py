import unittest
from algorithms.tree_dfs import dfs, dfs_q
from structures.tree import Node


class TestTreeDfs(unittest.TestCase):

    def test_dfs(self):
        root = Node(0, [
            Node(1, [
                Node(3, [
                    Node(5)
                ]),
                Node(4)
            ]),
            Node(2)
        ])

        items_list = dfs(root)
        items_list_q = dfs_q(root)
        self.assertListEqual(
            items_list, [0, 1, 3, 5, 4, 2], "Выходной массив не соответствует")

        self.assertListEqual(
            items_list_q, [0, 1, 3, 5, 4, 2], "Выходной массив не соответствует")
